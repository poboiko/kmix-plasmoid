/*
 * KMix -- KDE's full featured mini mixer
 *
 * Copyright 2011 Igor Poboiko <igor.poboiko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "mixerapplet.h"

#include <QListWidgetItem>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusPendingCall>

#include <KIcon>
#include <KConfigDialog>
#include <KConfigGroup>
#include <KToolInvocation>
#include <KProcess>

#include <Plasma/ToolTipManager>

K_EXPORT_PLASMA_APPLET(mixerapplet, MixerApplet)

MixerApplet::MixerApplet(QObject *parent, const QVariantList &args) 
	: Plasma::PopupApplet(parent, args)
	, m_masterSourceName()
	, m_preferredControlsOrder()
	, m_preferredControls()
	, m_availableControls()
	, m_widgets()
	, m_mainWidget(0)
	, m_popup(0)
	, m_mainLayout(0)
{
	Plasma::ToolTipManager::self()->registerWidget(this);
	setHasConfigurationInterface(true);
	setAspectRatioMode(Plasma::IgnoreAspectRatio);
	setPopupIcon("kmixdocked_error");
}

MixerApplet::~MixerApplet() 
{
	qDeleteAll(m_widgets);
	m_widgets.clear();

	m_availableControls.clear();
	m_preferredControls.clear();
	m_preferredControlsOrder.clear();
	m_playbackStreams.clear();
	m_captureStreams.clear();
}

void MixerApplet::init() 
{
	m_popup = new QGraphicsWidget(this);
	m_mainLayout = new QGraphicsLinearLayout(Qt::Horizontal, m_popup);
	configChanged();
	dataEngine("mixer")->connectSource("Mixers", this);
	// Setting actions
	QAction *phononAction = new QAction(this);
	phononAction->setIcon(KIcon("configure"));
	phononAction->setText(i18nc("context menu action", "Configure Sound System"));
	connect(phononAction, SIGNAL(triggered(bool)), this, SLOT(openPhononConfig()));
	addAction("configure_phonon", phononAction);

	QAction *kmixAction = new QAction(this);
	kmixAction->setIcon(KIcon("kmix"));
	kmixAction->setText(i18nc("context menu action", "Show KMix Window"));
	connect(kmixAction, SIGNAL(triggered(bool)), this, SLOT(showKMixWindow()));
	addAction("show_kmix", kmixAction);

	QAction *masterAction = new QAction(this);
	masterAction->setIcon(KIcon("configure"));
	masterAction->setText(i18nc("context menu action", "Select Master Control"));
	connect(masterAction, SIGNAL(triggered(bool)), this, SLOT(showSelectMasterControl()));
	addAction("select_master", masterAction);
}

void MixerApplet::createConfigurationInterface(KConfigDialog *parent) 
{
	/* UI magic */
	QWidget *widget = new QWidget(parent);
	m_ui.setupUi(widget);
	parent->addPage(widget, i18nc("KCM tab title", "General"), icon());

	/* Initializing UI with available and preferred controls */
	Q_FOREACH(const QString &preferred, m_preferredControlsOrder) {
		QListWidgetItem* item = new QListWidgetItem(m_ui.selectDevicesWidget->selectedListWidget());
		ControlInfo& c = m_preferredControls[preferred];
		item->setIcon(KIcon(c.iconName));
		item->setText(c.settingsName);
		item->setData(Qt::UserRole, c.source);
		if (!m_availableControls.contains(c.source))
			item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
	}
	bool isPulseAudio = false;
	Q_FOREACH(ControlInfo c, m_availableControls) {
		if (m_preferredControls.contains(c.source))
			continue;
		if (c.source.startsWith(QLatin1String("PulseAudio::Playback_Streams")) || c.source.startsWith(QLatin1String("PulseAudio::Capture_Streams"))) {
			isPulseAudio = true;
			continue;
		}
		QListWidgetItem* item = new QListWidgetItem(m_ui.selectDevicesWidget->availableListWidget());
		item->setIcon(KIcon(c.iconName));
		item->setText(c.settingsName);
		item->setData(Qt::UserRole, c.source);
	}

	m_ui.addStreamsBox->setChecked(m_autoAddStreams);
	m_ui.addStreamsBox->setVisible(isPulseAudio);
	
	m_ui.horizontalBtn->setChecked(m_orientation == Qt::Horizontal);
	m_ui.verticalBtn->setChecked(m_orientation == Qt::Vertical);

	connect(m_ui.addStreamsBox, SIGNAL(toggled(bool)), parent, SLOT(settingsModified()));
	connect(m_ui.selectDevicesWidget, SIGNAL(added(QListWidgetItem*)), parent, SLOT(settingsModified()));
	connect(m_ui.selectDevicesWidget, SIGNAL(removed(QListWidgetItem*)), parent, SLOT(settingsModified()));
	connect(m_ui.horizontalBtn, SIGNAL(toggled(bool)), parent, SLOT(settingsModified()));
	connect(m_ui.verticalBtn, SIGNAL(toggled(bool)), parent, SLOT(settingsModified()));
	connect(parent, SIGNAL(okClicked()), this, SLOT(configAccepted()));
	connect(parent, SIGNAL(applyClicked()), this, SLOT(configAccepted()));
}

void MixerApplet::configAccepted() {
	m_preferredControls.clear();
	m_preferredControlsOrder.clear();

	QStringList sources;
	QStringList icons;
	QStringList settingsNames;
	int count = m_ui.selectDevicesWidget->selectedListWidget()->count();
	for (int i = 0; i < count; i++) {
		QListWidgetItem *item = m_ui.selectDevicesWidget->selectedListWidget()->item(i);
		ControlInfo c;
		c.settingsName = item->text();
		c.iconName = item->icon().name();
		c.source = item->data(Qt::UserRole).toString();
		m_preferredControlsOrder.append(c.source);
		m_preferredControls.insert(c.source, c);
		sources.append(c.source);
		icons.append(c.iconName);
		settingsNames.append(c.settingsName);
	}

	m_autoAddStreams = m_ui.addStreamsBox->isChecked();
	m_orientation = m_ui.horizontalBtn->isChecked() ? Qt::Horizontal : Qt::Vertical;

	KConfigGroup cg = config();
	cg.writeEntry("sources", sources);
	cg.writeEntry("icons", icons);
	cg.writeEntry("settingsNames", settingsNames);
	cg.writeEntry("autoAddStreams", m_autoAddStreams);
	cg.writeEntry("isHorizontal", m_orientation == Qt::Horizontal);
	updateWidgets();
	emit configNeedsSaving();
}

void MixerApplet::configChanged() 
{
	m_preferredControls.clear();
	m_preferredControlsOrder.clear();
	qDeleteAll(m_widgets);
	m_widgets.clear();

	KConfigGroup cg = config();
	/* Loading preferred controls */
	m_orientation = cg.readEntry("isHorizontal", true) ? Qt::Horizontal : Qt::Vertical;
	m_autoAddStreams = cg.readEntry("autoAddStreams", true);
	QStringList sources = cg.readEntry("sources", QStringList());
	QStringList icons = cg.readEntry("icons", QStringList());
	QStringList settingsNames = cg.readEntry("settingsNames", QStringList());
	for (int i = 0; (i < sources.length()) && (i < icons.length()) && (i < settingsNames.length()); i++) {
		ControlInfo c;
		c.source = sources[i];
		c.iconName = icons[i];
		c.settingsName = settingsNames[i];
		m_preferredControls.insert(c.source, c);
		m_preferredControlsOrder.append(c.source);
	}
	updateWidgets();
}

void MixerApplet::wheelEvent(QGraphicsSceneWheelEvent *event)
{
	if (m_mainWidget)
		m_mainWidget->doSliderWheelEvent(event);
}

void MixerApplet::dataUpdated(const QString &sourceName,
		const Plasma::DataEngine::Data &data) 
{
	/* possible variants: kmix closed; device added; device removed */
	if (sourceName == "Mixers")	{
		/* Reconnecting to master source */
		if (!m_masterSourceName.isNull())
			dataEngine("mixer")->disconnectSource(m_masterSourceName, this);
		/* Anyway we will reload all the information about available controls */
		m_availableControls.clear();
		m_playbackStreams.clear();
		m_captureStreams.clear();
		m_kmixRunning = data["Running"].toBool();
		/* KMix closed or last audio device unplugged */
		if (data["Mixers"].isNull()) {
			setStatus(Plasma::PassiveStatus);
			setPopupIcon("kmixdocked_error");
		} else {
			setStatus(Plasma::ActiveStatus);
			m_masterSourceName = data["Current Master Mixer"].toString() + '/' + data["Current Master Control"].toString();
			Q_FOREACH(const QVariant &mixer, data["Mixers"].toList()) {
				Plasma::DataEngine::Data mixerData = dataEngine("mixer")->query(mixer.toString());
				dataEngine("mixer")->connectSource(mixer.toString(),
							this);
				updateAvailableControls(mixer.toString(), mixerData);
			}
		}
		updateWidgets();
		/* TODO: update settings if opened */
	}
	/* Source for mixer updated;
	 * possible variants: control added, control removed, ballance changed, source removed */
	else if (!sourceName.contains('/')) {
		updateAvailableControls(sourceName, data);
		updateWidgets();
	}
	/* Source for control updated;
	 * possible variants: volume/mute state changed; icon changed; et cetera */
	else if (sourceName == m_masterSourceName) {
		/* this is source used to paint icon */
		int level = data["Volume"].toInt();
		if (level <= 0 || data["Mute"].toBool())
			setPopupIcon("audio-volume-muted");
		else if (level < 25)
			setPopupIcon("audio-volume-low");
		else if (level < 75)
			setPopupIcon("audio-volume-medium");
		else 
			setPopupIcon("audio-volume-high");
	}
}

void MixerApplet::updateAvailableControls(const QString &mixerSource, const Plasma::DataEngine::Data &mixerData) 
{
	QStringList controlsNames = mixerData["Controls"].toStringList();
	QStringList controlsIcons = mixerData["Controls Icons Names"].toStringList();
	QStringList controlsReadableNames = mixerData["Controls Readable Names"].toStringList();
	QString mixerReadableName = mixerData["Readable Name"].toString();

	bool isPlaybackStreams = mixerSource.startsWith(QLatin1String("PulseAudio::Playback_Streams"));
	bool isCaptureStreams = mixerSource.startsWith(QLatin1String("PulseAudio::Capture_Streams"));
	if (isPlaybackStreams)
		m_playbackStreams.clear();
	else if (isCaptureStreams)
		m_captureStreams.clear();
	if (!m_preferredControlsOrder.contains(m_masterSourceName))
		m_preferredControlsOrder.append(m_masterSourceName);
	/* Master source is ALWAYS preferred (but may not be in preferred list
	 * on first run, for example) and is ALWAYS available */
	if (!m_preferredControlsOrder.contains(m_masterSourceName))
		m_preferredControlsOrder.append(m_masterSourceName);
	dataEngine("mixer")->connectSource(m_masterSourceName, this);
	/* All length's MUST ALWAYS be the same, but let's play safe */
	for (int i = 0; (i < controlsNames.length()) && (i < controlsIcons.length()) && (i < controlsReadableNames.length()); i++) {
		ControlInfo c;
		c.source = mixerSource + '/' + controlsNames[i];
		c.iconName = controlsIcons[i];
		c.readableName = controlsReadableNames[i];
		c.settingsName = QString("%1 (%2)").arg(c.readableName, mixerReadableName);
		m_availableControls[c.source] = c;
		if (m_preferredControls.contains(c.source) || (m_masterSourceName == c.source))
			m_preferredControls[c.source] = c;
		if (isPlaybackStreams)
			m_playbackStreams.append(c);
		if (isCaptureStreams)
			m_captureStreams.append(c);
	}
	
}

void MixerApplet::addWidget(const QString &source) 
{
	ControlWidget *w = new ControlWidget(m_popup, this, source, m_orientation);
	m_widgets.append(w);
	m_mainLayout->addItem(w);

	if (source == m_masterSourceName)
		m_mainWidget = w;
}

void MixerApplet::updateWidgets() 
{
	m_mainLayout->setOrientation(m_orientation == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);

	qDeleteAll(m_widgets);
	m_widgets.clear();
	Q_FOREACH(const QString &preferred, m_preferredControlsOrder) {
		if (m_availableControls.contains(preferred))
			addWidget(preferred);
	}
	if (m_autoAddStreams) {
		Q_FOREACH(ControlInfo c, m_playbackStreams)
			addWidget(c.source);
		Q_FOREACH(ControlInfo c, m_captureStreams)
			addWidget(c.source);
	}
	updateToolTip();
	m_popup->adjustSize();
}

void MixerApplet::updateToolTip()
{
	if (Plasma::ToolTipManager::self()->isVisible(this)) 
		toolTipAboutToShow();
}

void MixerApplet::toolTipAboutToShow() 
{
	QString mainText;
	QString subText;

	int count = m_widgets.length();
	if (count == 0) {
		if (m_kmixRunning) {
			mainText.append(i18nc("tooltip", "No audio device found"));
			subText.append(i18nc("tooltip", "KMix could not find any audio device in your system"));
		} else {
			mainText.append(i18nc("tooltip", "Is KMix running?"));
			subText.append(i18nc("tooltip", "This applet can not work without KMix running. Please start it."));
		}
	}
	else
	{
		for (int i = 0; i < m_widgets.length(); i++)
		{
			ControlWidget *w = m_widgets[i];
			if (i > 0)
				subText.append("<br/>");
			if (count > 1)
			{
				/* FIXME: somehow work with _mixer_ readable name too 
				 * (because different controls can have same readable name
				 * as in PulseAudio backend) */
				if (w->muted() && w->hasMuteSwitch())
					subText.append(i18nc("tooltip: %1 is control name, %2 is volume level", "<b>%1</b><br/>Volume level at %2% (muted)", w->readableName(), w->volume()));
				else if (!w->recordSource() && w->hasCaptureSwitch())
					subText.append(i18nc("tooltip: %1 is control name, %2 is volume level", "<b>%1</b><br/>Volume level at %2% (not capture)", w->readableName(), w->volume()));
				else
					subText.append(i18nc("tooltip: %1 is control name, %2 is volume level", "<b>%1</b><br/>Volume level at %2%", w->readableName(), w->volume()));
			} 
			else
			{
				if (w->muted() && w->hasMuteSwitch())
					subText.append(i18nc("tooltip: %1 is volume level", "Volume level at %1% (muted)", w->volume()));
				else if (!w->recordSource() && w->hasCaptureSwitch())
					subText.append(i18nc("tooltip: %1 is volume level", "Volume level at %1% (not capture)", w->volume()));
				else
					subText.append(i18nc("tooltip: %1 is volume level", "Volume level at %1%", w->volume()));
			}
		}
	}
	Plasma::ToolTipContent c(mainText, subText, KIcon("kmix"));
	Plasma::ToolTipManager::self()->setContent(this, c);
}

QGraphicsWidget* MixerApplet::graphicsWidget() {
	if (m_widgets.count() == 0) {
		// TODO: show popup with error (action "show_kmix" which runs kmix can be used)
		return NULL;
	} else
		return m_popup;
}

QList<QAction*> MixerApplet::contextualActions() {
	QList<QAction*> rv;
	rv << action("configure_phonon");
	rv << action("show_kmix");
	rv << action("select_master");
	return rv;
}

void MixerApplet::openPhononConfig() {
	QStringList args;
	args << "kcm_phonon";
	KToolInvocation::kdeinitExec("kcmshell4", args);
}

void MixerApplet::showKMixWindow() {
	QDBusMessage msg = QDBusMessage::createMethodCall("org.kde.kmix",
			"/kmix/KMixWindow",
			"org.qtproject.Qt.QWidget",
			"show");
	QDBusConnection::sessionBus().asyncCall(msg);
}

void MixerApplet::showSelectMasterControl() {
	QDBusMessage msg = QDBusMessage::createMethodCall("org.kde.kmix", 
			"/kmix/KMixWindow/actions/select_master",
			"org.qtproject.Qt.QAction",
			"trigger");
	QDBusConnection::sessionBus().asyncCall(msg);
}
