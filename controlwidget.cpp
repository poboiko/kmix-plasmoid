/*
 * KMix -- KDE's full featured mini mixer
 *
 * Copyright 2011 Igor Poboiko <igor.poboiko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "controlwidget.h"
#include "mixerapplet.h"

#include <Plasma/Theme>

MySlider::MySlider(QGraphicsWidget *parent)
	: Plasma::Slider(parent)
	, m_volume(0)
	, m_mousePressed(false)
{
	blockSignals(true);

	m_timer = new QTimer(this);
	m_timer->setSingleShot(true);
	m_timer->setInterval(100);

	connect(m_timer, SIGNAL(timeout()), this, SLOT(slotTimerEvent()));
}

void MySlider::wheelEvent(QGraphicsSceneWheelEvent *event)
{
	blockSignals(false);
	Plasma::Slider::wheelEvent(event);
	m_mousePressed = true;
	m_timer->start();
}

void MySlider::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	m_mousePressed = true;
	blockSignals(!m_mousePressed);
	Plasma::Slider::mousePressEvent(event);
}

void MySlider::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	m_mousePressed = false;
	Plasma::Slider::mouseReleaseEvent(event);
	setValue(m_volume);
	blockSignals(!m_mousePressed);
}

void MySlider::slotTimerEvent()
{
	m_mousePressed = false;
	blockSignals(!m_mousePressed);
}

void MySlider::setVolume(int volume)
{
	m_volume = volume;
	if (!m_mousePressed)
		setValue(m_volume);
}

ControlWidget::ControlWidget(QGraphicsWidget *parent, MixerApplet *applet, const QString &source, Qt::Orientation orientation)
	: QGraphicsWidget(parent)
	, m_source(source)
	, m_volume(0)
	, m_muted(true)
	, m_hasMuteSwitch(false)
	, m_recordSource(false)
	, m_hasCaptureSwitch(false)
	, m_orientation(orientation)
	, m_applet(applet)
{
	m_service = m_applet->dataEngine("mixer")->serviceForSource(source);
	m_service->setParent(this);
	Plasma::DataEngine::Data data = m_applet->dataEngine("mixer")->query(source);

	m_readableName = data["Readable Name"].toString();
	m_hasMuteSwitch = data["Can Be Muted"].toBool();
	m_hasCaptureSwitch = data["Has Capture Switch"].toBool();
	
	m_label = new Plasma::Label(this);
	m_label->setText(m_readableName);
	
	m_slider = new MySlider(this);
	m_slider->setOrientation(m_orientation);
	if (m_orientation == Qt::Horizontal)
		m_slider->setMinimumWidth(150);
	else
		m_slider->setMinimumHeight(150);
	connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(slotValueChanged(int)));

	m_muteWidget = new Plasma::IconWidget(this);
	m_muteWidget->setVisible(m_hasMuteSwitch);
	m_muteWidget->setPreferredIconSize(QSizeF(24, 24));
	m_muteWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	connect(m_muteWidget, SIGNAL(clicked()), this, SLOT(slotMuteClicked()));

	m_captureCheckBox = new Plasma::CheckBox(this);
	m_captureCheckBox->setText(i18nc("checkbox", "Capture"));
	m_captureCheckBox->setVisible(m_hasCaptureSwitch);
	m_captureCheckBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
	connect(m_captureCheckBox, SIGNAL(toggled(bool)), this, SLOT(slotCaptureClicked(bool)));

	m_icon = new Plasma::IconWidget(this);
	m_icon->setAcceptsHoverEvents(false);
	m_icon->setPreferredIconSize(QSizeF(24, 24));
	m_icon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	m_icon->setIcon(data["Icon"].value<QIcon>());

	/*
	 * +-----------------------------------+
	 * |Readable name <-------->|[x]Capture|
	 * +-----------------------------------+
	 * |[icon]|[=========>========]|[muted]|
	 * +-----------------------------------+
	 *
	 * XXX: muteWidget is always there so all the controlWidgets
	 * have the same layout; but it is sometimes hidden
	 */
	if (m_orientation == Qt::Horizontal) {
		m_label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
		m_label->setWordWrap(false);
		m_label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		
		QGraphicsLinearLayout *mainLayout = new QGraphicsLinearLayout(Qt::Vertical, this);
		QGraphicsLinearLayout *topLayout = new QGraphicsLinearLayout(Qt::Horizontal, mainLayout);
		topLayout->addItem(m_label);
		if (m_hasCaptureSwitch) 
			topLayout->addItem(m_captureCheckBox);
		QGraphicsLinearLayout *bottomLayout = new QGraphicsLinearLayout(Qt::Horizontal, mainLayout);
		bottomLayout->addItem(m_icon);
		bottomLayout->addItem(m_slider);
		bottomLayout->addItem(m_muteWidget);
		mainLayout->addItem(topLayout);
		mainLayout->addItem(bottomLayout);
		setLayout(mainLayout);
	} else {
		m_label->setAlignment(Qt::AlignHCenter);
		m_label->setWordWrap(true);
		/* FIXME: horizontal size policy actually should be minimum; but due to some strange Qt bug,
		 * minimum size policy leads to preferred size and vice versa */
		m_label->setPreferredWidth(m_label->minimumWidth());
		m_label->setPreferredHeight(m_label->nativeWidget()->heightForWidth(m_label->minimumWidth()));
		m_label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

		QGraphicsLinearLayout *mainLayout = new QGraphicsLinearLayout(Qt::Vertical, this);
		mainLayout->addItem(m_icon);
		mainLayout->setAlignment(mainLayout->itemAt(0), Qt::AlignHCenter);
		mainLayout->addItem(m_label);
		mainLayout->setAlignment(mainLayout->itemAt(1), Qt::AlignHCenter);
		mainLayout->addItem(m_slider);
		mainLayout->setAlignment(mainLayout->itemAt(2), Qt::AlignHCenter);
		/* mute widget is always there, but not always visible */
		if (m_hasCaptureSwitch)
			mainLayout->addItem(m_captureCheckBox);
		else
			mainLayout->addItem(m_muteWidget);
		mainLayout->setAlignment(mainLayout->itemAt(3), Qt::AlignHCenter);
		setLayout(mainLayout);
	}

	dataUpdated(source, data);
	m_applet->dataEngine("mixer")->connectSource(source, this);
}

ControlWidget::~ControlWidget() 
{
}


void ControlWidget::slotValueChanged(int value) {
	// FIXME: it may be not a good idea to set volume every time slider moves
	KConfigGroup op = m_service->operationDescription("setVolume");
	op.writeEntry("level", value);
	m_service->startOperationCall(op);
}

void ControlWidget::slotMuteClicked() {
	KConfigGroup op = m_service->operationDescription("setMute");
	op.writeEntry("muted", !m_muted);
	m_service->startOperationCall(op);
}

void ControlWidget::slotCaptureClicked(bool value) {
	KConfigGroup op = m_service->operationDescription("setRecordSource");
	op.writeEntry("recordSource", value);
	m_service->startOperationCall(op);
}

void ControlWidget::dataUpdated(const QString &sourceName, 
		const Plasma::DataEngine::Data &data) 
{
	Q_UNUSED(sourceName);
	
	m_readableName = data["Readable Name"].toString();
	m_label->setText(m_readableName);

	m_slider->setVolume(data["Volume"].toInt());
	if (data["Volume"].toInt() != m_volume) {
		m_volume = data["Volume"].toInt();
		m_applet->updateToolTip();
	}
	
	if (data["Mute"].toBool())
		setMuteIcon("audio-volume-muted");
	else
		setMuteIcon("audio-volume-high");
	if (m_muted != data["Mute"].toBool()) {
		m_muted = data["Mute"].toBool();
		m_applet->updateToolTip();
	}

	m_captureCheckBox->setChecked(data["Record Source"].toBool());
	if (m_recordSource != data["Record Source"].toBool()) {
		m_recordSource = data["Record Source"].toBool();
		m_applet->updateToolTip();
	}
}

const QString& ControlWidget::readableName()
{
	return m_readableName;
}

void ControlWidget::doSliderWheelEvent(QGraphicsSceneWheelEvent *event)
{
	m_slider->wheelEvent(event);
}

int ControlWidget::volume()
{
	return m_volume;
}

bool ControlWidget::muted()
{
	return m_muted;
}

bool ControlWidget::hasMuteSwitch()
{
	return m_hasMuteSwitch;
}

bool ControlWidget::recordSource()
{
	return m_recordSource;
}

bool ControlWidget::hasCaptureSwitch()
{
	return m_hasCaptureSwitch;
}

void ControlWidget::setMuteIcon(const QString& icon)
{
	const QString svgPath("icons/audio");
	if (Plasma::Theme::defaultTheme()->imagePath(svgPath).isEmpty()) 
		m_muteWidget->setIcon(icon);
	else
		m_muteWidget->setSvg(svgPath, icon);
}
