/*
 * KMix -- KDE's full featured mini mixer
 *
 * Copyright 2011 Igor Poboiko <igor.poboiko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CONTROLWIDGET_HEADER
#define CONTROLWIDGET_HEADER

#include <QObject>
#include <QString>
#include <QGraphicsGridLayout>
#include <QGraphicsSceneWheelEvent>
#include <QTimer>

#include <Plasma/DataEngine>
#include <Plasma/Slider>
#include <Plasma/Label>
#include <Plasma/IconWidget>
#include <Plasma/CheckBox>

class MixerApplet;

class MySlider : public Plasma::Slider
{
Q_OBJECT
public:
	MySlider(QGraphicsWidget *parent);
	void wheelEvent(QGraphicsSceneWheelEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void setVolume(int volume);
public Q_SLOTS:
	void slotTimerEvent();
private:
	int m_volume;
	bool m_mousePressed;
	QTimer *m_timer;
};

class ControlWidget : public QGraphicsWidget
{
Q_OBJECT
public:
	ControlWidget(QGraphicsWidget *parent, MixerApplet *applet, const QString &source, Qt::Orientation orientation);
	~ControlWidget();

	int volume();
	bool muted();
	bool hasMuteSwitch();
	bool recordSource();
	bool hasCaptureSwitch();
	const QString& readableName();
	void doSliderWheelEvent(QGraphicsSceneWheelEvent *event);
protected Q_SLOTS:
	void dataUpdated(const QString &sourceName,
			const Plasma::DataEngine::Data &data);
	void slotValueChanged(int value);
	void slotMuteClicked();
	void slotCaptureClicked(bool value);
private:
	QString m_source;

	QString m_readableName;
	int m_volume;
	bool m_muted;
	bool m_hasMuteSwitch;
	bool m_recordSource;
	bool m_hasCaptureSwitch;

	Qt::Orientation m_orientation;

	Plasma::Label *m_label;
	MySlider *m_slider;
	Plasma::IconWidget *m_muteWidget;
	Plasma::CheckBox *m_captureCheckBox;
	Plasma::IconWidget *m_icon;

	Plasma::Service *m_service;

	MixerApplet *m_applet;
	
	void setMuteIcon(const QString& icon);
};

#endif // CONTROLWIDGET_HEADER
