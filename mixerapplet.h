/*
 * KMix -- KDE's full featured mini mixer
 *
 * Copyright 2011 Igor Poboiko <igor.poboiko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MIXERAPPLET_HEADER
#define MIXERAPPLET_HEADER

#include <Plasma/PopupApplet>
#include <Plasma/DataEngine>

#include <QGraphicsWidget>
#include <QGraphicsLinearLayout>
#include <QGraphicsSceneWheelEvent>

#include "ui_mixerConfig.h"
#include "controlwidget.h"

typedef struct {
	QString settingsName;
	QString source;
	QString iconName;
	QString readableName;
} ControlInfo;

class MixerApplet : public Plasma::PopupApplet
{
Q_OBJECT
public:
	MixerApplet(QObject *parent, const QVariantList &args);
	~MixerApplet();

	void init();
	QList<QAction*> contextualActions();
	void updateToolTip();
public Q_SLOTS:
	void dataUpdated(const QString& sourceName,
			const Plasma::DataEngine::Data &data);
	void configAccepted();
	void configChanged();
	void toolTipAboutToShow();
	
	void openPhononConfig();
	void showKMixWindow();
	void showSelectMasterControl();

	QGraphicsWidget *graphicsWidget();
private:
	Ui::MixerConfigWidget m_ui;

	QString m_masterSourceName;
	QStringList m_preferredControlsOrder;
	QHash<QString,ControlInfo> m_preferredControls;
	QHash<QString,ControlInfo> m_availableControls;
	QList<ControlInfo> m_playbackStreams;
	QList<ControlInfo> m_captureStreams;
	
	QList<ControlWidget*> m_widgets;
	ControlWidget* m_mainWidget;

	bool m_autoAddStreams;
	Qt::Orientation m_orientation;

	QGraphicsWidget *m_popup;
	QGraphicsLinearLayout *m_mainLayout;

	bool m_kmixRunning;

	void addWidget(const QString &source);
	void createConfigurationInterface(KConfigDialog *parent);
	void updateAvailableControls(const QString &mixerSource, const Plasma::DataEngine::Data &mixerData);

	void updateWidgets();
	void wheelEvent(QGraphicsSceneWheelEvent *event);
};

#endif // MIXERAPPLET_HEADER
